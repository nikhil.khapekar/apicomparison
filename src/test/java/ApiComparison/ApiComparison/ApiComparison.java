package ApiComparison.ApiComparison;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class ApiComparison {
	Properties prop;

	@BeforeMethod
	public void runConfiguration() throws IOException {

		prop=new Properties();
		FileInputStream ip=new FileInputStream("C:\\Users\\Nikhil\\eclipse-workspace\\ApiComparison\\src\\test\\resources\\config.properties");
		prop.load(ip);
	}


	@Test
	public void testMethod() throws FileNotFoundException {

		ClearFileData CFD=new ClearFileData();
		CFD.clearFileData(prop.getProperty("captureApiResponse1"));
		CFD.clearFileData(prop.getProperty("captureApiResponse2"));

		ReadFileAndWriteResponse RFAWR=new ReadFileAndWriteResponse();
		RFAWR.readFileAndWriteResponse(prop.getProperty("readApiFile1"), prop.getProperty("captureApiResponse1"));
		RFAWR.readFileAndWriteResponse(prop.getProperty("readApiFile2"), prop.getProperty("captureApiResponse2"));

		CompareResponse CR =new CompareResponse();
		CR.compareResponse(prop.getProperty("captureApiResponse1"), prop.getProperty("captureApiResponse2"));
	}



}
