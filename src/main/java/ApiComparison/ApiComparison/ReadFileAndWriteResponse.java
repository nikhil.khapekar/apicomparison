package ApiComparison.ApiComparison;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ReadFileAndWriteResponse {

	public static String str;
	static FileInputStream fis;
	static Scanner sc;

	//Getting response and writing into the file
	public void getResponse(String path) throws MalformedURLException, IOException {
		URLConnection connection = new URL(str).openConnection();
		connection.setRequestProperty("User-Agent", "Chrome/84.0.4147.135");
		connection.connect();

		BufferedReader r  = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
			sb.append(line);
		}

		String response=sb.toString();
		System.out.println(response);

		JsonParser parse = new JsonParser();
		JsonObject jobj = (JsonObject)parse.parse(response); 


		System.out.println(jobj);
		File file = new File(path);
		FileWriter fw = new FileWriter(file,true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(response);

		bw.newLine();
		fw.write("\n");
		bw.close();

	}

	//Read API File, hitting API, getting response and writing response in file
	public void readFileAndWriteResponse(String pathReadFile, String writeApiResponse) {
		try  
		{  
			//the file to be opened for reading  
			fis=new FileInputStream(pathReadFile);       

			sc=new Scanner(fis);    //file to be scanned  


			//returns true if there is another line to read  
			while(sc.hasNextLine())  

			{  
				str=sc.nextLine();
				System.out.println(str);    
				getResponse(writeApiResponse);

			}  
			sc.close();     //closes the scanner  
		}  
		catch(IOException e)  
		{  
			e.printStackTrace();  
		} 
	}


}
