package ApiComparison.ApiComparison;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CompareResponse {

	public void compareResponse(String captureApiResponse1, String captureApiResponse2) {
		ObjectMapper om=new ObjectMapper();
		List <String> data1=null;
		List <String> data2=null;

		try {
			data1=java.nio.file.Files.readAllLines(Paths.get(captureApiResponse1));
		}catch(IOException e) {}


		try {
			data2=java.nio.file.Files.readAllLines(Paths.get(captureApiResponse2));
		}catch(IOException e) {}

		JSONArray jsonArray;
		JSONArray jsonArray1;

		try {
			jsonArray=new JSONArray(data1);
			jsonArray1=new JSONArray(data2);

			for(int i=0;i<=jsonArray.length()-1;i++) {

				try {
					Map<String, Object> m1=(Map<String, Object>)(om.readValue(jsonArray.getString(i).toString(),Map.class));
					Map<String, Object> m2=(Map<String, Object>)(om.readValue(jsonArray1.getString(i).toString(),Map.class));

					System.out.println(m1);
					System.out.println(m2);

					if(m1.equals(m2)){
						System.out.println("Response is equal");
					}else {
						System.out.println("Response is not equal");
					}
				} 

				catch(JSONException | JsonProcessingException e) {}
			}

		}catch(com.google.gson.JsonParseException e) {}

	}




}
