package ApiComparison.ApiComparison;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ClearFileData {

	//Clear File before writting response
	public void clearFileData(String path) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(path);
		writer.print("");
		writer.close();
	}
}
